<?php
$server = "127.0.0.1";
$user = "root";
$password = "";
$db = "users";

$conn = mysqli_connect($server, $user, $password, $db);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$email = $_POST['email'];

if ($email == '') {
	echo 'Email field was empty, nothing added to db<br>';
	echo '<a href="index.html">Try again</a>';
} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	echo '<b>' . $email . '</b> is not a valid email<br>';
	echo '<a href="index.html">Try again</a>';
} else {
	$sql = "INSERT INTO test (email)
	VALUES ('$email')";

	if (mysqli_query($conn, $sql)) {
	    echo '<b>' . $email . '</b> successfully added to db<br>';
		echo '<a href="index.html">Do it again</a>';
	} else {
	    echo 'Error: ' . $sql . '<br>' . mysqli_error($conn);
	}

}

mysqli_close($conn);

?>
